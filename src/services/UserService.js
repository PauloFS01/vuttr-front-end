import http from './HttpService';

export default {
    async auth (user) {
        try {
            const result = await http().post('/auth', user);
            console.log('resultado é: ' + result);
            return result.data;            
        } catch (error) {
            throw error;
        }

    }
}
import axios from 'axios';
import store from '../store'

const createAxios = () => {
  const token = store.state.user.token || '';  
    return axios.create({
      baseURL: process.env.VUE_APP_API_URL,
      headers: {
       'Content-Type':'application/json',
       'x-access-token': token
      },      
    });    
  };
  export default createAxios;
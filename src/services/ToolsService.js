import http from './HttpService';
export default {
    async create (data) {
        try {
            const result = await http().post('/tools', data);
            return result.data;           
        } catch (error) {
            throw error;
        }

    },
    async getListTools () {
        try {
            const result = await http().get('/tools');
            return result.data;            
        } catch (error) {
           console.log('serviçe arror: ' + error);
        }
    },
    async searchTools (search) {
        const result = await http().get('/tools?q=' + search);
        return result.data;          
    },
    async tagSearch (tag) {
        const result = await http().get('/tools?tags_like=' + tag);
        return result.data;
    },
    async delete (id) {
        try {
            const result = await http().delete('/tools/' + id);
            return result.data;            
        } catch (error) {
            console.log(error);
            throw error;
        }

    },
}
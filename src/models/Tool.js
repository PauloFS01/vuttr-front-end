export default class tool {
    constructor (data = {}) {
        this.title = data.title;
        this.link = data.link;
        this.description = data.description;
        this.tags = data.tags;
    }
}
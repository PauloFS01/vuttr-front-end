import Vue from 'vue'
import Vuex from 'vuex'
import Http from '@/services/ToolsService.js'
import HttpUser from '@/services/UserService.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {},
    tools:[],
    show: false,
  },
  mutations: {
    SET_USER (state, result) {
      state.user = result;
    },
    SET_TOOLS (state, result) {
      state.tools = result;
    },
    SET_SHOW (state) {
      state.show = !state.show;
    }    
  },
  actions: {
    async login (store, user){
      store.commit('SET_USER', await HttpUser.auth(user));     
    },
    logout (store) {
      store.commit('SET_USER', {}); 
    },
    async getTools (store) {
      try {
        store.commit('SET_TOOLS', await Http.getListTools());
      } catch (error) {
        console.log('store error: ' + error);      
      }     
    },
    async searchTag (store, tag) {
      store.commit('SET_TOOLS', await Http.tagSearch(tag));
    },
    async searchTools (store, search) {
      store.commit('SET_TOOLS', await Http.searchTools(search));
    },
    showModal (store) {
      store.commit('SET_SHOW');
    }
  },
  getters: {
    isLogged (state) {
      return !(state.user.token === undefined || (new Date(state.user.expiredAt).getTime() < new Date().getTime()));
    },
    getUser (state) {
      return state.user;
    },
    loadingTools (state) {
      return state.tools;
    },
    getShow (state) {
      return state.show;
    }
  }
})

# vuttr-updated
> Essa é uma aplicação simples de front-end, onde o usuário poderá logar e inserir dados (que serão salvos em uma api externa), podendo posteriormente busca-los atravêz do título descrição ou tag.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## libraries and technologies used 

* [x] **[VueJS 2.x](https://github.com/vuejs/vue)**
* [x] [Vuex](https://github.com/vuejs/vuex)
* [x] [Vue-router](https://github.com/vuejs/vue-router)
* [x] [axios](https://github.com/mzabriskie/axios)
* [x] [bootstrap-vue](https://bootstrap-vue.js.org/docs)
* [x] [SCSS](http://sass-lang.com/)
* [x] [fontawesome-free](https://fontawesome.com/how-to-use/on-the-web/referencing-icons/basic-use)

## Vuex
Dentro do arquivo store.js esta o state, com dados que estaram disponíveis para toda a aplicação e as actions com requisições que resultam em dados que estarão disponiveis através do state.

## Bootstrap-vue
Disponibiiza componentes como cards e header, baseados em vuejs e na biblioteca front-end CSS Bootstrap V4.

## Vue Router
Dentro do arquivo router.js estão configuradas as rotas da aplicação.

## Directory structure
```

   +---public|
   +---src
   |   +---assets
   |   +---components
   |       +---LandingPage
   |       |       CardTools.vue.js
   |       |       ConfirmComponent.vue.js
   |       |       CustomHeader.vue.js
   |       |       Form.vue.js
   |       |       LandingPage.vue.js
   |       +---Shared
   |               Modal.vue
   |
   +---models
   |       Tools.js
   +---services
   |       HttpService.js
   |       ToolsService.js
   |       UserService.js
   +---themes
   +---views
   |       About.vue
   |       Home.vue
   |       Login.vue
   |    App.vue
   |    main.js
   |    router.js
   |    store.js
   |    
   package.json
```